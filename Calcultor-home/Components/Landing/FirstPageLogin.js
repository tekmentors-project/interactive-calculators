//authentication
function initFirebase() {

    var config = {
        apiKey: "AIzaSyC5F_rVxOM0RiIBUeKyE4fWto3OwuJL4DY",
        authDomain: "kalculate-f5d42.firebaseapp.com",
        databaseURL: "https://kalculate-f5d42.firebaseio.com",
        projectId: "kalculate-f5d42",

    };
    firebase.initializeApp(config);
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            // var displayName = firebaseUser.displayName;
            var email = firebaseUser.email;
            var uid = firebaseUser.uid;
            var obj = {
                uid: uid,
                email: email
            };
            localStorage.setItem("userObject", JSON.stringify(obj));
            var user = JSON.parse(localStorage.getItem("userObject"))
            console.log("user logged in");
            $("#tryThis").click();
            document.getElementById("asideBar").style.display = "block";
            $("#asideBar").append("<div class='dashboard'>History</div>");
            firebase.database().ref("/users/" + user["uid"] + "/templates/").once('value', function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    var childKey = childSnapshot.key;
                    console.log(childSnapshot.key);
                    $(".dashboard").append(`<div class='card-aside' id='${childKey}'><p class='card-aside-date'>${childData.dateOfCreation}</p><p class='card-aside-name'>${childData.name}</p></div>`);
                });
            });

            document.getElementById('nav3').style.display = "block";
            document.getElementById('nav1').style.display = "none";
            document.getElementById('nav2').style.display = "none";
        } else {
            console.log("not logged in");
        }
    });
}


//Alert Divs
function alertMessage(message) {
    console.log("hi");
    $("#success-alert").show();
    var div = document.getElementById("success-alert-message");
    div.innerHTML += message;
    setTimeout(function () {
        div.style.display = "none";
    }, 5000);
}
function populateHistory() {
    var userObject = localStorage.getItem("userObject");
    console.log(userObject);
}

//ADD LOGIN EVENT
function try_login() {
    const emailUser = $('#emailUser').val();
    const passwordUser = $('#passwordUser').val();
    const auth = firebase.auth();
    const promise = auth.signInWithEmailAndPassword(emailUser, passwordUser);
    $("#errorMessage").append("");
    promise.catch(e => $("#errorMessage").append(e.message));
    var notfiy = "User successfully logged in";
    setInterval(myfunction(), 1000000000);
    function myfunction()
    {
        if (document.getElementById("errorMessage").innerHTML == "") {
            alertMessage(notfiy);
            populateHistory();
        }
    }
    
}

function try_signUp() {
    var email = $('#email').val();
    var password = $('#password').val();
    const auth = firebase.auth();
    const promise = auth.createUserWithEmailAndPassword(email, password);
    $("#dismiss").click();
    promise.catch(e => {
        console.log(e.message);
        // $('#successMessage').text(e);
    });
    var notfiy = " User account created successfully";
    alertMessage(notfiy);
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    firebase.auth().onAuthStateChanged(function (user) {
        console.log(user);
        if (user) {
            var userObject = JSON.parse(localStorage.getItem("userObject"));
            var id = userObject["uid"];
            var details = {
                firstName: firstName,
                lastName: lastName,
                id: id
            };
            console.log(userObject["uid"]);
            firebase.database().ref("/users/" + userObject["uid"] + "/").set(details);
            console.log(userObject["uid"]);
        } else {
            // No user is signed in.
        }
    });
}

function logout() {
    firebase.auth().signOut();
    window.open("../Landing/FirstPage.html", "_parent");
    // window.onbeforeunload = function() { return "Your work will be lost."; };

    document.getElementById('nav3').style.display = "none";
    document.getElementById('nav1').style.display = "block";
    document.getElementById('nav2').style.display = "block";
    document.getElementById("asideBar").style.display = "none";
}



