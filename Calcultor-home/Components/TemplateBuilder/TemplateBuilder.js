const WELCOME_TAB_NAME = 'WELCOME_TAB_NAME';
const CALCULATOR_TAB_NAME = 'CALCULATOR_TAB_NAME';
const RESULTS_TAB_NAME = 'RESULTS_TAB_NAME';

const WORKSPACE_TABS = [
    {
        name: WELCOME_TAB_NAME,
        bodyid: 'welcome',
        updateTab: updateWelcomeTab,
        next: null,
        previous: null
    },
    {
        name: CALCULATOR_TAB_NAME,
        bodyid: 'main',
        updateTab: updateCalculatorTab,
        next: null,
        previous: null
    },
    {
        name: RESULTS_TAB_NAME,
        bodyid: 'result',
        updateTab: updateResultsTab,
        next: null,
        previous: null
    }

];
const WELCOME_TAB = WORKSPACE_TABS[0];
const CALCULATOR_TAB = WORKSPACE_TABS[1];
const RESULTS_TAB = WORKSPACE_TABS[2];
WELCOME_TAB.next = CALCULATOR_TAB;
CALCULATOR_TAB.previous = WELCOME_TAB;
CALCULATOR_TAB.next = RESULTS_TAB;
RESULTS_TAB.previous = CALCULATOR_TAB;


let activeWorkspaceTab = WORKSPACE_TABS.find(tab => tab.name = WELCOME_TAB);

const ALGO_TAB_NAME = 'ALGO_TAB_NAME';
const EDITOR_TAB_NAME = 'EDITOR_TAB_NAME';

const LEFTPANE_TABS = [
    {
        name: ALGO_TAB_NAME,
        bodyid: 'formula-input',
        updateTab: updateAlgorithmTab
    },
    {
        name: EDITOR_TAB_NAME,
        bodyid: 'select-input-type',
        updateTab: updateEditorTab
    }
];
const ALGO_TAB = LEFTPANE_TABS[0];
const EDITOR_TAB = LEFTPANE_TABS[1];

let activeLeftPaneTab = EDITOR_TAB;

let activeQuestion = null;

/**----Initial UI render on window load----**/
window.onload = loadUI();
async function loadUI() {

    calculatorModel = await fetchData();
    initializeUI();
}

function initializeUI() {
    initHeader();
    initLeftPane();
    initWorkspace();
}

function initHeader() {
    addHeaderEventListners();
    updateHeaderView();
}

function addHeaderEventListners() {

}

function updateHeaderView() {

}

function initLeftPane() {
    addLeftPaneEventListners();
    updateLeftPaneView();
}

function addLeftPaneEventListners() {
    fillInputType();
    $("#input-type-container").on('input', function () {
        if (activeQuestion.type === QUESTION_TYPE_NUMERIC) {
            activeQuestion.value = document.getElementById("default-value-text").value;
        } else {
            Object.keys(activeQuestion.options).forEach((key) => {
                var newKey = document.getElementById(`val${key}`).value;
                var newValue = document.getElementById(`name${key}`).value;
                activeQuestion.options[key].value = newKey;
                activeQuestion.options[key].label = newValue;
            });
        }


        updateWorkspaceView();
    });
}

function updateLeftPaneView() {
    if (activeQuestion) {
        $("#input-type-container").html('');
        questionMap[activeQuestion.type].populateLeftSectionDOM();
    }

}

function updateAlgorithmTab() {

}

function updateEditorTab() {

}

function initWorkspace() {
    addWorkSpaceListners();
    updateWorkspaceView();
}

function addWorkSpaceListners() {
    $("#back-button").click(function () {

        activeWorkspaceTab = activeWorkspaceTab.previous;
        updateWorkspaceView();
    });
    $("#forward-button").click(function () {

        activeWorkspaceTab = activeWorkspaceTab.next;
        updateWorkspaceView();
    });
    $("#workspace-container").on('input', function () {
        event.stopPropagation;
        let id = (event.target.id);
        if (id == "sub_heading" || id == "heading" || id == "button_text") {
            calculatorModel.welcome_screen[id] = $(event.target).text();
        }
        else {
            id = (id).replace("QUES_", "");
            calculatorModel.section[id].title = $(event.target).text();
        }
    });

}

function updateWorkspaceView() {

    WORKSPACE_TABS.forEach(tab => $('#' + tab.bodyid).hide());
    $("#back-button").prop("disabled", false);
    $("#forward-button").prop("disabled", false);
    if (activeWorkspaceTab.next === null) {
        $("#forward-button").prop("disabled", true);
    }
    if (activeWorkspaceTab.previous === null) {
        $("#back-button").prop("disabled", true);
    }
    $(`#${activeWorkspaceTab.bodyid}`).show();
    activeWorkspaceTab.updateTab();


}

function updateWelcomeTab() {
    $("#welcome-screen").html('');

    $("#welcome-screen").append("<h1 class='cover-heading' id='heading' contenteditable='true'></h1>");
    $("#heading").append(calculatorModel.welcome_screen.heading);

    $("#welcome-screen").append("<p class='lead' id='sub_heading' contenteditable='true'></p>");
    $("#sub_heading").append(calculatorModel.welcome_screen.sub_heading);

    $("#welcome-screen").append("<p class='lead'></p>").append("<button type='button' class='btn btn-danger' id='button_text' contenteditable='true'></button>");
    $("#button_text").append(calculatorModel.welcome_screen.button_text);

}

function updateCalculatorTab() {
    if (calculatorModel.section) {
        $("#questionnaire").html('');
        Object.keys(calculatorModel.section)
            .forEach(questionno => {
                const question = calculatorModel.section[questionno];
                if (question) {
                    $("#questionnaire").append("<div class='form-group col-9 col-lg-9' id='" + questionno + "'></div>");
                    $("#" + questionno).append("<label id='QUES_" + questionno + "' data-url='QUES_" + questionno + "' contenteditable='true'></label>");
                    $("#QUES_" + questionno).append(question.title);
                    questionMap[question.type].populateWorkspaceDOM(questionno);


                    $("#questionnaire").append("<div id='editPanel" + questionno + "' class='col-3 col-lg-3' style='text-align: center;padding-top: 15.5px;'></div>");
                    $("#editPanel" + questionno + "").append("<div class='btn-group' id='editPanelButtonGroup" + questionno + "'></div>");
                    if (activeQuestion !== question) {
                        $("#editPanelButtonGroup" + questionno + "").append("<button id='edit" + questionno + "' style='background-color:#7272E5' class='btn a-btn-slide-text' onclick=editInput('" + questionno + "')><span class='fa fa-pencil' aria-hidden='true'></span></button>");
                    }

                    $("#editPanelButtonGroup" + questionno + "").append("<button id='delete" + questionno + "' style='background-color:#7272E5' class='btn a-btn-slide-text' onclick=deleteInput('" + questionno + "')><span class='fa fa-remove' aria-hidden='true'></span></button>");
                    $("button").click(function (event) {
                        event.preventDefault();
                    });
                }
            });
    }
}

function updateResultsTab() {
    $("#result").html('');
    $("#result").append("<div class='container'><div class='row'><div class='col-6 col-lg-6'><form id='resultPage'></form></div><div id='displayResult' class='col-6 col-lg-6 text-center'></div></div></div>");
    $("#resultPage").append("<div class='form-group'><label for='email'>Email address:</label><input type='email' class='form-control' id='email'></div><button type='submit' class='btn' style='background-color: #B2B2FF'>View Result</button>");
    $("#displayResult").html(calculatorModel.result.displayText);
    $("#resultPage").append("<label>Enter the prefix: </label>")
    $("#resultPage").append(`<div id='suffix>${calculatorModel.result.prefix}</div>`)

}

function fillInputType() {
    let inputTypes = $('#selectBox');
    inputTypes.html('');
    Object.keys(questionMap).forEach(key => {
        inputTypes.append(
            $('<option></option>').val(key).html(questionMap[key].displayLabel)
        );
    })

}

function changeFunc() {
    let selectBox = document.getElementById("selectBox");
    let selectedValue = selectBox.options[selectBox.selectedIndex].value;
    const newDefaultQuestion = questionMap[selectedValue].getdefaultQuestion();
    activeQuestion.type = newDefaultQuestion.type;
    if (newDefaultQuestion.options)
        activeQuestion.options = newDefaultQuestion.options;
    if (newDefaultQuestion.value)
        activeQuestion.value = newDefaultQuestion.value;
    activeQuestion.title = newDefaultQuestion.title;
    updateLeftPaneView();
    updateWorkspaceView();
}

function viewSelectInput() {
    document.getElementById("select-input-type").style.display = "block";
    document.getElementById("viewAlgorithmInputBtn").style.display = "inline-block";
    document.getElementById("formula-input").style.display = "none";
    activeQuestion = questionMap[QUESTION_TYPE_NUMERIC].getdefaultQuestion();
    let newid = 0;
    Object.keys(calculatorModel.section).forEach(key => {
        if (parseInt(key, 10) > newid) {
            newid = parseInt(key, 10);
        }
    });
    ++newid;
    calculatorModel.section[newid.toString()] = activeQuestion;
    activeQuestion.id = newid;
    activeWorkspaceTab = CALCULATOR_TAB;
    updateLeftPaneView();
    updateWorkspaceView();


}

function editInput(id) {
    activeQuestion = calculatorModel.section[id];
    activeQuestion.id = calculatorModel.section[id].id;
    updateLeftPaneView();
    updateWorkspaceView();
}

function deleteInput(id) {
    delete calculatorModel.section[id];
    updateLeftPaneView();
    updateWorkspaceView();
}

function addOption() {
    var newOptionid = Object.keys(activeQuestion.options).length;
    activeQuestion.options[newOptionid] = {};
    calculatorModel.section[activeQuestion.id].options[newOptionid].label = "New Label";
    calculatorModel.section[activeQuestion.id].options[newOptionid].value = "-1";
    updateWorkspaceView();
    updateLeftPaneView();
}

function removeOption() {
    var id = Object.keys(activeQuestion.options).length - 1;
    delete activeQuestion.options[id];
    delete calculatorModel.section[activeQuestion.id].options[id];
    updateWorkspaceView();
    updateLeftPaneView();
}

/**----Function to copy URL when going live----**/
function myFunction() {
    var specific = document.getElementById("myInput");
    calculatorModel.title = copyText;
    // copyText.select();
    // document.execCommand("copy");

    // var tooltip = document.getElementById("myTooltip");
    // tooltip.innerHTML = "Copied";
}

/**----Function to go live----**/
function goLive() {
    var name = $("#myInput").val();
    debugger;
    var template = calculatorModel;
    calculatorModel.dateOfCreation = localStorage.getItem('DOC');
    calculatorModel.name=name;
    calculatorModel.result.prefix=document.getElementById("prefix").value;
    var newPostKey = firebase.database().ref().child("published_templates").push().key;
    var updates = {};
    postData = {
        name: name,
        template: template
    }
    updates['/published_templates/' + newPostKey] = postData;
    firebase.database().ref().update(updates);
    firebase.database().ref(localStorage.getItem('seturl')).set(calculatorModel);
    window.open("../Templates/LivePage.html?name=" + name);
}

// /**----View the div to select input type for new algorithm----**/
function viewAlgorithmInput() {
    document.getElementById("formula-input").style.display = "block";
    document.getElementById("select-input-type").style.display = "none";
    activeWorkspaceTab = CALCULATOR_TAB;
    updateWorkspaceView();
    buildAlgorithm();
}

/**----Function to build algorithm----**/
function addToAlgo(id) {
    document.getElementById('algorithmInput').value += (document.getElementById(id).value);
}
function buildAlgorithm() {
    $('#formula-input').html('');
    for (var i = 0, j = 0; i < calculatorModel.section.length; i++) {

        if (calculatorModel.section[i]) {
            $("#formula-input").append(`<button type='button' style='background-color:#B2B2FF;' class='btn btn-circle btn-lg' value='INPUT_${i}'  id='INPUT_${i}' onclick=addToAlgo('INPUT_${i}')>${j + 1}</button>`);
            j++;
        }
    }
    $("#formula-input").append("<div class='btn-group btn-matrix'><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='plus' value='+' onclick=addToAlgo('plus')>+</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='minus' value='-' onclick=addToAlgo('minus')>-</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='multiply' value='*' onclick=addToAlgo('multiply')>*</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='divide' value='/' onclick=addToAlgo('divide')>/</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='questionMark' value='?' onclick=addToAlgo('questionMark')>?</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='colon' value=':' onclick=addToAlgo('colon')>:</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='or' value='||' onclick=addToAlgo('or')>||</button><button type='button' style='background-color:white; border-color:#B2B2FF' class='btn btn-default' id='and' value='&&' onclick=addToAlgo('and')>&&</button><button type='button' style='background-color:white; border-color:#B2B2FF' id='modulus' value='%' class='btn btn-default' onclick=addToAlgo('modulus')>%</button></div>");
    $("#formula-input").append("<div class='form-group'><label for='comment'>Algorithm</label><textarea class='form-control' rows='3' id='algorithmInput'></textarea></div>");
    $("#formula-input").append("<button onclick='implementAlgorithm()' style='background-color:#B2B2FF' class='btn'>CREATE</button>");
}
/**----Function to implement algorithm----**/
function implementAlgorithm() {
    var algorithm = document.getElementById("algorithmInput").value;
    calculatorModel.result["algo"] = algorithm;
}


