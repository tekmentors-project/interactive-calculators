var config = {
    apiKey: "AIzaSyC5F_rVxOM0RiIBUeKyE4fWto3OwuJL4DY",
    authDomain: "kalculate-f5d42.firebaseapp.com",
    databaseURL: "https://kalculate-f5d42.firebaseio.com",
    projectId: "kalculate-f5d42",
    storageBucket: "kalculate-f5d42.appspot.com",
    messagingSenderId: "753822206781"
};
firebase.initializeApp(config);
var database = firebase.database();
let algorithm;
const WELCOME_TAB_NAME = 'WELCOME_TAB_NAME';
const CALCULATOR_TAB_NAME = 'CALCULATOR_TAB_NAME';
const WORKSPACE_TABS = [
    {
        name: WELCOME_TAB_NAME,
        bodyid: 'welcome',
        updateTab: updateWelcomeTab
    },
    {
        name: CALCULATOR_TAB_NAME,
        bodyid: 'main',
        updateTab: updateCalculatorTab
    },
];
const WELCOME_TAB = WORKSPACE_TABS[0];
const CALCULATOR_TAB = WORKSPACE_TABS[1];


let activeWorkspaceTab = WORKSPACE_TABS.find(tab => tab.name = WELCOME_TAB_NAME);

let activeQuestion = null;


/**----Initial UI render on window load----**/
window.onload = loadUI();
function loadUI() {
    let url=window.location.href;
    url=url.split('=')[1];
    fetchData(url).then(data => {
        console.log(data);
        calculatorModel = data.template;
        $('#questionnaire').change(function () {
            var result = countTotal();
            calculatorModel.result.value = result;
            $("#finalResult").text(result);
        });
        initializeUI();
    })
}

function fetchData(url) {
    return fetchTemplate(url)
        .then(function (result) {
            let template = null;
            Object.keys(result)
                .forEach(key => {
                    template = result[key];
                });
            return template;
        });
}


function fetchTemplate(url) {
    let query = firebase.database().ref("published_templates").orderByChild('name').equalTo(url);
    return query.once("value").then(function (snapshot) {
        console.log(snapshot);
        return snapshot.val();
    })
}

function initializeUI() {
    initCalculator();
}

function initCalculator() {
    addCalculatorListners();
    updateCalculatorView();
}

function addCalculatorListners() {
    $("#button_text").click(function () {
        activeWorkspaceTab = CALCULATOR_TAB;
        var result = countTotal();
        calculatorModel.result.value = result;
        $("#finalResult").text(result);
        updateCalculatorView();
    });
}

function updateCalculatorView() {

    $('#' + CALCULATOR_TAB.bodyid).hide();
    $('#' + WELCOME_TAB.bodyid).hide();
    $(`#${activeWorkspaceTab.bodyid}`).show();
    activeWorkspaceTab.updateTab();
}

function updateWelcomeTab() {
    $("#welcome-screen").html('');
    $("#welcome-screen").append("<p class='lead' id='sub_heading'></p>");
    $("#sub_heading").append(calculatorModel.welcome_screen.sub_heading);

    $("#welcome-screen").append("<h1 class='cover-heading' id='heading'></h1>");
    $("#heading").append(calculatorModel.welcome_screen.heading);

    $("#welcome-screen").append("<p class='lead'></p>").append("<button type='button' class='btn btn-danger' id='button_text'></button>");
    $("#button_text").append(calculatorModel.welcome_screen.button_text);

}

function updateCalculatorTab() {
    if (calculatorModel.section) {
        $("#questionnaire").html('');
        Object.keys(calculatorModel.section)
            .forEach(questionno => {
                const question = calculatorModel.section[questionno];
                if (question) {
                    $("#questionnaire").append("<div class='form-group col-9 col-lg-9' id='" + questionno + "'></div>");
                    $("#" + questionno).append("<label id='QUES_" + questionno + "' data-url='QUES_" + questionno + "'></label>");
                    $("#QUES_" + questionno).append(question.title);
                    questionMap[question.type].populateWorkspaceDOM(questionno);
                }
            });
        algorithm = calculatorModel.result.algo;
    }
}

function updateResultsTab() {
    $("#result").html('');
    $("#result").append("<div class='container'><div class='row'><div class='col-6 col-lg-6'><form id='resultPage'></form></div><div id='displayResult' class='col-6 col-lg-6 text-center'></div></div></div>");
    $("#resultPage").append("<div class='form-group'><label for='email'>Email address:</label><input type='email' class='form-control' id='email'></div><button type='submit' class='btn btn-primary'>View Result</button>");
    $("#displayResult").html(calculatorModel.result.displayText);

}
function viewResult() {
    if (document.getElementById('email').value != "")
        $("#result-div").show();
    else { alertMessage(); }

}
function countTotal() {
    var temp_algo = algorithm;
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    var counter = 0;
    $("*[data-common='true']").each(function (indiv) {
        var temp = $(this)[0].value;
        var temp_id = $(this)[0].id;
        if (temp_algo.includes(temp_id)) {
            temp = (temp).replaceAll('"', '');
            temp_algo = temp_algo.replaceAll(temp_id, temp);
        }
        counter = eval(temp_algo);
        console.log(counter);
    });
    return counter;
}

/***********************************************/
function alertMessage() {
    $("#email-alert").show();
    var div = document.getElementById("email-alert-message");
    setTimeout(function () {
        div.style.display = "none";
    }, 5000);
}