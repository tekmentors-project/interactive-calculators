let calculatorModel = {};


const QUESTION_TYPE_NUMERIC = 'QUESTION_TYPE_NUMERIC';
const QUESTION_TYPE_DROPDOWN = 'QUESTION_TYPE_DROPDOWN';
const QUESTION_TYPE_RADIO = 'QUESTION_TYPE_RADIO';
const QUESTION_TYPE_CHECKBOX = 'QUESTION_TYPE_CHECKBOX';


let questionMap = {};
questionMap[QUESTION_TYPE_NUMERIC] = {
    displayLabel: 'Numeric',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append(`<input type='number' class='form-control' data-common='true'>`);
        $("#" + quesNo).find("input").attr("value", question.value);
    },
    populateLeftSectionDOM: function () {
        $("#input-type-container").append("<label>Default Value</label><input type='number' class='form-control' id='default-value-text'/>");
        document.getElementById("addOptBtn").style.display = "none";
        document.getElementById("removeOptBtn").style.display = "none";
    },
    getdefaultQuestion: function () {
        return {
            "id": 0,
            "title": "How often do you want to drive? (No. of hours per week)",
            "type": "QUESTION_TYPE_NUMERIC",
            "value": "1"
        }
    }
};
questionMap[QUESTION_TYPE_DROPDOWN] = {
    displayLabel: 'Dropdown',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append("<select class='custom-select' data-common='true'></select>");
        for (i in question.options) {
            if (question.options[i] != null) {
                let label = question.options[i].label;
                let value = question.options[i].value;
                $("#" + quesNo).find("select").append($('<option></option>').val(value).html(label));
            }
        }

    },
    populateLeftSectionDOM: function () {
        Object.keys(activeQuestion.options).forEach((key) => {
            if (activeQuestion.options[key] !== null) {
                $("#input-type-container").append(`<input type='text' class='form-control group-input-name' value='' id='name${key}'/><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'>Option</span></div><input type='text' class='form-control group-input-value' value='' id='val${key}' /></div>`);
                document.getElementById(`name${key}`).value = activeQuestion.options[key].label;
                document.getElementById(`val${key}`).value = activeQuestion.options[key].value;
            }
        });
        document.getElementById("addOptBtn").style.display = "inline-block";
        document.getElementById("removeOptBtn").style.display = "inline-block";
        document.getElementById("addOptBtn").disabled = false;
        document.getElementById("removeOptBtn").disabled = false;
    },
    getdefaultQuestion: function () {
        return {
            "id": 0,
            "title": "Where do you want to drive?",
            "type": "QUESTION_TYPE_DROPDOWN",
            "options": [{
                "label": "China",
                "value": 20
            }, {
                "label": "Europe",
                "value": 30
            }, {
                "label": "North America",
                "value": 40
            }, {
                "label": "Australia",
                "value": 50
            }, {
                "label": "India",
                "value": 80
            }]
        }
    }
};
questionMap[QUESTION_TYPE_RADIO] = {
    displayLabel: 'Radio',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append("<br>");
        $("#" + quesNo).append(`<div id='ANSWER_${quesNo}' class='form-group'></div>`);
        // $(`#ANSWER_${quesNo}`).html('');
        let flag = 0;
        for (i in question.options) {
            if (question.options[i] != null) {
                let label = question.options[i].label;
                let value = question.options[i].value;
                let count = flag++;
                $(`#ANSWER_${quesNo}`).append("<div class='form-check-inline'><input type='radio' data-common='true' class='form-check-input'><label class='form-check-label' id='ANSWER_" + quesNo + "RADIO_" + count + "'></label></div>");
                $(`#ANSWER_${quesNo}`).find("input").val(value);
                $("#ANSWER_" + quesNo + "RADIO_" + count).append(label);
            }
        }
    },
    populateLeftSectionDOM: function () {
        Object.keys(activeQuestion.options).forEach((key) => {
            if (activeQuestion.options[key] !== null) {
                $("#input-type-container").append(`<input type='text' class='form-control group-input-name' value='' id='name${key}'/><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'>Option</span></div><input type='text' class='form-control group-input-value' value='' id='val${key}' /></div>`);
                document.getElementById(`name${key}`).value = activeQuestion.options[key].label;
                document.getElementById(`val${key}`).value = activeQuestion.options[key].value;
            }

        });
        document.getElementById("addOptBtn").style.display = "inline-block";
        document.getElementById("removeOptBtn").style.display = "inline-block";
        document.getElementById("addOptBtn").disabled = false;
        document.getElementById("removeOptBtn").disabled = false;
    },
    getdefaultQuestion: function () {
        return {
            "id": 0,
            "title": "How sociable are you?",
            "type": "QUESTION_TYPE_RADIO",
            "options": [{
                "label": "Not at all",
                "value": 0
            }, {
                "label": "Love humans!",
                "value": 10
            }, {
                "label": "Meh",
                "value": 5
            }, {
                "label": "Just enough",
                "value": 7
            }]

        }
    }

};
questionMap[QUESTION_TYPE_CHECKBOX] = {
    displayLabel: 'Checkbox',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append("<br>");
        $("#" + quesNo).append(`<div id='ANSWER_${quesNo}' class='form-group'></div>`);
        // $(`#ANSWER_${quesNo}`).html('');
        let flag = 0;
        for (i in question.options) {
            if (question.options[i] != null) {
                let label = question.options[i].label;
                let value = question.options[i].value;
                let count = flag++;
                $(`#ANSWER_${quesNo}`).append("<div class='form-check-inline'><input type='checkbox' data-common='true' class='form-check-input'><label class='form-check-label' id='ANSWER_" + quesNo + "CHECK_" + count + "'></label></div>");
                $(`#ANSWER_${quesNo}`).find("input").val(value);
                $("#ANSWER_" + quesNo + "CHECK_" + count).append(label);
            }
        }
    },
    populateLeftSectionDOM: function () {
        // $("#input-type-container").html('');
        Object.keys(activeQuestion.options).forEach((key) => {
            if (activeQuestion.options[key] !== null) {
                $("#input-type-container").append(`<input type='text' class='form-control group-input-name' value='' id='name${key}'/><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group-text'>Option</span></div><input type='text' class='form-control group-input-value' value='' id='val${key}' /></div>`);
                document.getElementById(`name${key}`).value = activeQuestion.options[key].label;
                document.getElementById(`val${key}`).value = activeQuestion.options[key].value; 
            }

        });
        document.getElementById("addOptBtn").style.display = "inline-block";
        document.getElementById("removeOptBtn").style.display = "inline-block";
        document.getElementById("addOptBtn").disabled = false;
        document.getElementById("removeOptBtn").disabled = false;
    },
    getdefaultQuestion: function () {
        return {
            "id": 0,
            "title": "How sociable are you?",
            "type": "QUESTION_TYPE_CHECKBOX",
            "options": [{
                "label": "Not at all",
                "value": 0
            }, {
                "label": "Love humans!",
                "value": 10
            }, {
                "label": "Meh",
                "value": 5
            }, {
                "label": "Just enough",
                "value": 7
            }]

        }
    }

};

