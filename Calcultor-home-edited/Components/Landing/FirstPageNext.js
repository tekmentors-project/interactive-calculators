window.onload = function () {
    
    initFirebase();
    loadUI();
}

var templateData;
async function fetchData() {
    // read our JSON
    let response = await fetch(URL["X"] + ".json");
    let data = await response.json();
    return data;
}
async function loadUI() {
    templateData = await fetchData();
    prepareHTML(templateData);
}

function prepareHTML(data) {
    console.log(data);
}
async function createInstance() {
    let response = await fetch(URL["Z"] + ".json?shallow=true");
    let data = await response.json();
    return data;
}
async function createTemplate() {

    if (document.getElementById("nav3").style.display == "block") {
        var userObject = JSON.parse(localStorage.getItem("userObject"));
        firebase.database().ref("/users/" + userObject["uid"] + "/templates").push(templateData).then((snap) => {
            const templatekey = snap.key;
            let date = new Date().toLocaleString();
            console.log(date);
            localStorage.setItem('userObject', userObject["uid"]);
            localStorage.setItem('userurl', URL["Z"] + "/" + userObject["uid"] + "/templates/" + templatekey);
            localStorage.setItem('seturl', "/users/" + userObject["uid"] + "/templates/" + templatekey);
            localStorage.setItem('DOC', date);
            window.open("../TemplateBuilder/TemplateBuilder.html", "_parent");
        });
    } else {

    }
}
function displayFirstPage() {
    window.open("FirstPage.html", "_parent");
}


