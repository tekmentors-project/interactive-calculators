/**----Function to copy URL when going live----**/
async function fetchData() {
    // read our JSON
    USER_URL[localStorage.getItem('userkey')] = localStorage.getItem('userurl');
    let response = await fetch(localStorage.getItem('userurl') + ".json");
    let data = await response.json();
    return data;
}

/**----Function to save data----**/
function saveData() {
    $.ajax({
        url: "",
        data: calculatorModel,
        type: 'PUT',
        async: true,
        content: 'text/json',
        success: function(response) {
            console.log('done');
        },
        error: function(error) {
            console.log('error');
        }
    })
}
