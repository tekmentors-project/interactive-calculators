let calculatorModel = {};


const QUESTION_TYPE_NUMERIC = 'QUESTION_TYPE_NUMERIC';
const QUESTION_TYPE_DROPDOWN = 'QUESTION_TYPE_DROPDOWN';
const QUESTION_TYPE_RADIO = 'QUESTION_TYPE_RADIO';
const QUESTION_TYPE_CHECKBOX = 'QUESTION_TYPE_CHECKBOX';


let questionMap = {};
questionMap[QUESTION_TYPE_NUMERIC] = {
    displayLabel: 'Numeric',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append(`<input type='number' class='form-control' id='INPUT_${quesNo}' data-common='true'>`);
        $("#" + quesNo).find("input").attr("value", question.value);
    }
};
questionMap[QUESTION_TYPE_DROPDOWN] = {
    displayLabel: 'Dropdown',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append(`<select class='custom-select' id='INPUT_${quesNo}' data-common='true'></select>`);
        for (i in question.options) {
            let label = question.options[i].label;
            let value = question.options[i].value;
            $("#" + quesNo).find("select").append($('<option></option>').val(value).html(label));
        }

    }
};
questionMap[QUESTION_TYPE_RADIO] = {
    displayLabel: 'Radio',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append("<br>");
        $("#" + quesNo).append(`<div id='ANSWER_${quesNo}' class='form-group'></div>`);
        // $(`#ANSWER_${quesNo}`).html('');
        let flag = 0;
        for (i in question.options) {
            let label = question.options[i].label;
            let value = question.options[i].value;
            let count = flag++;
            $(`#ANSWER_${quesNo}`).append(`<div class='form-check-inline'><input type='radio' name='button-group' data-common='true' id='INPUT_${quesNo}' class='form-check-input'><label class='form-check-label' id='ANSWER_${quesNo}RADIO_${count}'></label></div>`);
            $(`#ANSWER_${quesNo}`).find("input").val(value);
            $("#ANSWER_" + quesNo + "RADIO_" + count).append(label);
        }
    }
};
questionMap[QUESTION_TYPE_CHECKBOX] = {
    displayLabel: 'Checkbox',
    populateWorkspaceDOM: function (quesNo) {
        const question = calculatorModel.section[quesNo];
        $("#" + quesNo).append("<br>");
        $("#" + quesNo).append(`<div id='ANSWER_${quesNo}' class='form-group'></div>`);
        // $(`#ANSWER_${quesNo}`).html('');
        let flag = 0;
        for (i in question.options) {
            let label = question.options[i].label;
            let value = question.options[i].value;
            let count = flag++;
            $(`#ANSWER_${quesNo}`).append(`<div class='form-check-inline'><input type='checkbox' data-common='true' id='INPUT_${quesNo}' class='form-check-input'><label class='form-check-label' id='ANSWER_${quesNo}CHECK_${count}'></label></div>`);
            $(`#ANSWER_${quesNo}`).find("input").val(value);
            $("#ANSWER_" + quesNo + "CHECK_" + count).append(label);
        }
    }
};

